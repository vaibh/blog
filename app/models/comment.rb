class Comment < ActiveRecord::Base 
  belongs_to :user
  
  belongs_to :post

  validates :post, presence: true
  #validates :post, presence: {message: "Post is not valid,post is not saved"}

  validates :body, presence: true
  #validates :body, presence: {message: "Body is not valid,post is not saved"}

end
